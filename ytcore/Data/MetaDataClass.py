from .DataClass import Data

class MetaData(Data):
    Type = "metadata"
    MetaType = "unknown"
