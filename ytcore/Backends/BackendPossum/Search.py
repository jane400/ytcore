#
#    This file is part of ytcore.
#
#    ytcore is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ytcore is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with ytcore.  If not, see <http://www.gnu.org/licenses/>.
#
from ytcore.Backends.BackendStub import BackendStubSearch

from . import utils as helper
from .API import API

_filter_dict = {
        "upload": {
            "lasthour": str(),
            "today": str(),
            "week": str(),
            "month": str(),
            "year": str()
            },
        "type": {
            "video": str(),
            "channel": str(),
            "playlist": str(),
            "movie": str(),
            "show": str()
            },
        "length": {
            "short": str(),
            "long": str()
            },
        "features": {
            "live": str(),
            "4K": str(),
            "HD": str(),
            "Subtitles": str(),
            "Creative Commons": str(),
            "360": str(),
            "VR180": str(),
            "3D": str(),
            "HDR": str(),
            "Location": str(),
            "Purchased": str()
            },
        "sorting": {
            "relevance": str(),
            "upload": str(),
            "view": str(),
            "rating": str()
            },
        "other": {
            "force": str()
            }
    }


class BackendPossumSearch(BackendStubSearch):
    parent = None
    api:API = None

    def __init__(self, parent):
        super().__init__(parent)
        self.api = self.parent.api

    def get(self, caller):
        def responseRaw(request_raw):
            for i in request_raw:
                if "response" in i:
                    response_raw = i["response"]
            if "refinements" in response_raw:
                caller._recommended_available = True
                caller._recommended_searchquerys = response_raw["refinements"]
            return response_raw

        def sectionListRaw():
            content_raw = response_raw["contents"]
            primary_content_raw = content_raw["twoColumnSearchResultsRenderer"]["primaryContents"]
            section_list_raw = primary_content_raw["sectionListRenderer"]
            return section_list_raw, raw

        raw = self.api.search(caller.get_searchquery())
        caller._searched = True
        response_raw = responseRaw(raw)
        sec_list, raw = sectionListRaw()
        sectionListContentsRaw = sec_list["contents"]
        caller._results, info = helper.parse_content(sectionListContentsRaw)
        caller._success = len(caller._results) > 0
        for i in info:
            # switch; case; break; thats one feature i miss in python
            if i["type"] == "suggestedQuery":
                caller._suggestedSearch = True
                caller._suggestedQuery = i["suggestion"]
            elif i["type"] == "replacedQuery":
                caller._replacedSearch = True
                caller._suggestedQuery = i["suggestion"]
            elif i["type"] == "continuation":
                caller._continueable = True
                caller.__continue_token = i["token"]
            elif i["type"] == "error":
                caller._success = False
                #FIXME: Proper Error Handling
                print(i)
                return

    def get_continue(self, caller):
        caller._continueable = False
        raw = self.api.search_continue(caller.__continue_token)
        parsed, info, mode = helper.parse_continue_response(raw)
        if mode == "append":
            caller._continue_success = True
            caller._continue_results = parsed
            for p in parsed:
                caller._results.append(p)
        elif mode == "error":
            caller._continue_success = False
            return list()
        for i in info:
            if i["type"] == "continuation":
                caller._continueable = True
                caller.__continue_token = i["token"]
        return parsed


    # FIXME: CHECKS FOR VALID RESULT
    # FIXME: More checks.
    # FIXME: Write into self.errormsg
    # Returns True if everything is valid. Otherwise False
    def _check_inital(self):
        error = False
        if not self.check_searchterm(self.searchterm):
            self._error_invalid_input(self.searchterm)
            error = True
        if not self.check_filter(self._filter):
            self._error_invalid_filter(self._filter)
            error = True
   

    def _check_inital_end(self):
        if(self._check_response()):
            return True
        return False

    def _check_response(self):
        if(len(self._response) > 0):
            return True
        return False

    @staticmethod
    def check_searchterm(searchterm):
        if(len(searchterm) > 0):
            return True
        return False

    @staticmethod
    def check_filter(filters):
        invalid = list()
        for i in filters:
            for u in _filter_dict:
                if not i in u:
                    invalid.append(i)
        if len(invalid) == 0:
            return False, invalid
        else:
            return True, invalid

    @staticmethod
    def get_filters_by_category(category):
        return _filter_dict[category.lower()]

    @staticmethod
    def get_filters():
        out = []
        for i in _filter_dict:
            for u in i:
                out.append(u)
        return out

    @staticmethod
    def get_categories():
        return list(_filter_dict)
