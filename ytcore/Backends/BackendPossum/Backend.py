from .API import API
from .Search import BackendPossumSearch
from .Video import BackendPossumVideo
from ytcore.Backends import BackendStub

class BackendPossum(BackendStub):
    downloader = None
    api = None
    
    # Public
    video = None
    search = None
    
    features = {
            "VideoPage": True,
            "Video": False,
            "VideoPageComments": False,
            "SearchPage": True,
            "SearchPageFilter": False,
            "ChannelPage": False,
            "Feeds": False
            }


    def __init__(self, core):
        super().__init__(core)
        self.downloader = core.get_downloader()
        self.api = API(self.downloader, self._YT_Headers)
        self.video = BackendPossumVideo(self)
        self.search = BackendPossumSearch(self)

    def update_localization(self, core):
        pass

    def get_languagelist(self):
        pass

    def get_countrylist(self):
        pass

    # Private:
    _YT_Headers = {
            'x-youtube-client-version': "2.20210210.08.00",
            'x-youtube-client-name': "1",
            "Accept-Language": "de-DE"  # Gets replaced.
        }
