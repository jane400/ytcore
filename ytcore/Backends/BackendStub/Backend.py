from .Search import BackendStubSearch
from .Video import BackendStubVideo

class BackendStub:
    # Public
    video: BackendStubVideo = None
    search: BackendStubSearch = None

    features = {
            "VideoPage": False,
            "Video": False,
            "VideoPageComments": False,
            "SearchPage": False,
            "SearchPageFilter": False,
            "ChannelPage": False,
            "Feeds": False
            }


    def __init__(self, core):
        pass

    def update_localization(self, core):
        pass

    def get_languagelist(self):
        pass

    def get_countrylist(self):
        pass

