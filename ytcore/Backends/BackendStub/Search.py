#
#    This file is part of yt_core.
#
#    yt_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    yt_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with yt_core.  If not, see <http://www.gnu.org/licenses/>.
#

class BackendStubSearch:
    parent = None

    def __init__(self, parent):
        self.parent = parent

    def get(self, caller):
        pass

    def get_continue(self, caller):
        pass

    @staticmethod
    def check_searchquery(searchterm):
        if(len(searchterm) > 0):
            return True
        return False
