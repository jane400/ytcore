#    This file is part of ytcore.
#
#    ytcore is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ytcore is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with ytcore.  If not, see <http://www.gnu.org/licenses/>.
#

class Page:
    """Baseclass of all pages.

    Note
    ----
    This class doesnt do anything. It's just here to declare the functions implemented by all pages.
    """

    _success = False

    def init(self):
        """Initializes the object.
        This typically fetches the data from the backend and fills
        the information here.

        Returns
        -------
        bool
            True if operation was sucessfull, False otherwise.
        """
        return self._success

    def get(self):
        """Returns the results.

        This method returns the results accomplished by init() or other methods.

        Returns
        -------
        NoneType
            The type is diffrent on every page, as every page has diffrent results.
        """
        return None
