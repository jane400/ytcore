#    This file is part of ytcore.
#
#    ytcore is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ytcore is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with ytcore.  If not, see <http://www.gnu.org/licenses/>.
#

from .PageClass import Page
from .Backends.BackendStub import BackendStubSearch

class SearchPage(Page):
    """Frontend for a SearchPage

    This Page deals with searches and requires therefor a searchquery.
    Optional settings are filters (not implemented yet).

    Note
    ----
    Initalizing pages manually is not supported and
    could break in future version due to internal API-changes.
    """
    _backend:BackendStubSearch = None

    _success = False
    _continue_success = False

    # User-Settings:
    _searchquery = str()


    # Info:
    _continueable = False
    _error = []
    _filters_available_b = False
    _recommended_searchquerys_available = False
    _replaced_search = False
    _suggested_search = False
    _searched = False

    # Data:
    _filters = []
    _filters_available = []
    _filters_selected = []
    _estimated_results = 0
    _results = []
    _continue_results = []
    _suggested_query = ""
    _recommended_searchquerys = []

    _internal_data = None
    # Used for storage like continuation-tokens by the backend.
    #   (the backends shouldn't store too specific data)
    # something thats applicibale for all pages should
    #  either be stored inside the backend or core
    #
    # Please don't store passwords in here,
    #  rather use platform specific keyrings like gnome-keyring-daemon.
    # Thanks! :)

    # Logic Methods
    def __init__(self, core, searchquery=str()):
        self.backend = core.get_backend().search
        #FIXME: is searchquery valid?
        self._searchquery = searchquery

    def init(self) -> bool:
        self.backend.get(self)
        return self._success

    def continuation(self) -> bool:
        if self._continueable:
            self.backend.get_continue(self)
        return self._continue_success

    # Get: Infos and data
    def is_continuable(self) -> bool:
        return self._continueable

    def is_searched(self) -> bool:
        return self._searched

    def is_suggesting_query(self) -> bool:
        return self._suggested_search

    def is_replaced_with_suggestion(self):
        return self._replaced_search

    def get_suggestion_query(self):
        return self._suggested_query

    def get_searchquery(self):
        return self._searchquery

    def has_recommended_queries(self):
        return self._recommended_searchquerys_available

    def get_recommended_queries(self):
        return self._recommended_searchquerys

    def get_results_count_estimated(self):
        return self._estimated_results

    def get_results(self):
        return self._results

    def get_continuation_results(self):
        return self._continue_results

    def get(self):
        return self._results

    # Static Methods
    @staticmethod
    def check_searchquery(searchquery: str):
        #FIXME: Currently defaults to True, implement the searchquery-check
        # Should ask the backend if it's valid
        return True
